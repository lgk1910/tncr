from mmdet.apis import init_detector, inference_detector, show_result_pyplot
import mmcv
import numpy as np
import cv2
import glob
import os
import time
from tqdm import tqdm


class TableDetectClassify:
    '''
    Class này có nhiệm vụ phát hiện và phân loại cụ thể từng loại table ('full_lined', 'merged_cells', 'nolines','partial_lined','partial_lined_merged_cells')
    SOTA đang sử dụng mmdetection
    Repo tìm hiểu: https://github.com/open-mmlab/mmdetection
    @config_file: file config của model classification
    @checkpoint_file: weight của model classification
    '''
    def __init__(self,config_file,checkpoint_file):
        self.config_file = config_file
        self.checkpoint_file = checkpoint_file
        #Khởi tạo model
        self.model = init_detector(self.config_file , self.checkpoint_file, device='cuda:0')
        # elif method == 'triton':
        #     self.model = TableDetectClassifyServing(model_name='mmdetection', url='127.0.0.1:8001')

    def infer(self,img,thresh=0.5):
        polygons = []
        scoress = []
        labels_list = np.array([])
        img_org = np.copy(img)
        while True:
            #inference
            result = inference_detector(self.model, img)

            #Lấy tọa độ bbox và nhãn tương ứng
            if isinstance(result, tuple):
                bbox_result, segm_result = result
                if isinstance(segm_result, tuple):
                    segm_result = segm_result[0]  
            else:
                bbox_result, segm_result = result, None
        
            bboxes = np.vstack(bbox_result)

            labels = [np.full(bbox.shape[0], i, dtype=np.int32) for i, bbox in enumerate(bbox_result)]
            labels = np.concatenate(labels)

            assert bboxes is not None and bboxes.shape[1] == 5
            scores = bboxes[:, -1]
            inds = scores > thresh
            bboxes = bboxes[inds, :]
            labels = labels[inds]
            scores = scores[inds]

            
            labels_list = np.concatenate((labels_list,labels))

            if bboxes is not None:
                if len(bboxes) == 0:
                    break
                for i, bbox in enumerate(bboxes):
                    bbox_int = bbox.astype(np.int32)
                    poly = [[bbox_int[0], bbox_int[1]], [bbox_int[0], bbox_int[3]],
                                [bbox_int[2], bbox_int[3]], [bbox_int[2], bbox_int[1]]]
                    x_min, x_max, y_min, y_max = bbox_int[0], bbox_int[2], bbox_int[1], bbox_int[3]
                    np_poly = np.array(poly).reshape((4, 2))
                    tab_coors = (x_min, x_max, y_min, y_max)
                    polygons.append(np_poly)
                    scoress.append(scores[i])
                tmp = img[tab_coors[2]:tab_coors[3]+1, tab_coors[0]:tab_coors[1]+1, :]
                img[tab_coors[2]:tab_coors[3]+1, tab_coors[0]:tab_coors[1]+1, :] = np.full_like(tmp, 255)
        
        labels = labels_list
        if len(polygons) == 0:
            # NO TABLE DETECTED
            return [], [], [], []
        
        else:
            # print(f'len polygons {len(polygons)}')
            sub_imgs = []
            lbs = []
            for i, polygon in enumerate(polygons):
                # CLASSIFY
                x1,y1 = polygon[0]
                x2,y2 = polygon[1]
                x3,y3 = polygon[2]
                x4,y4 = polygon[3]

                #crop table 
                # img = cv2.imread(img)
                sub_img = img_org[y1:y3,x1:x3]

                dict_labels = ['full_lined', 'merged_cells', 'nolines','partial_lined','partial_lined_merged_cells']

                label = dict_labels[int(labels[i])]
                sub_imgs.append(sub_img)
                lbs.append(label)

            return sub_imgs, lbs, polygons, scoress

config_file = '/home/ec2-user/pipeline_table_extraction/table_classification/pre-trained/cascade_rcnn/cascade_rcnn_x101_32x4d_fpn_1x_coco.py'
checkpoint_file = '/home/ec2-user/pipeline_table_extraction/table_classification/pre-trained/cascade_rcnn/model.pth'

classification = TableDetectClassify(config_file, checkpoint_file)
img_paths = glob.glob('/home/ec2-user/table_detect_survey/table_datatest_merge/*.jpg')
# img_paths = ['../imgs/table_test_anh_dat/80_0.jpg']
avg_times = []
for t in range(5,10):
    save_path = f'/home/ec2-user/table_detect_survey/table_datatest_merge/output/tmp/txt/thr0{t}'
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    inference_time = 0
    for i, img_path in enumerate(img_paths):
        img = cv2.imread(img_path)
        basename = os.path.basename(img_path)
        img_name = basename.split('.')[0]
        # print(f'img shape: {img.shape}')
        start = time.time()
        sub_imgs, labels, polygons, scores  = classification.infer(img, thresh=t/10)
        # print(f'len polygons == len scores??? {len(polygons)==len(scores)}={len(scores)}')
        inference_time += time.time() - start
        ltrb_label = ''
        img = cv2.imread(img_path) # comment this line if not drawing rectangle
        for i, polygon in enumerate(polygons):
            x1,y1 = polygon[0]  # xmin, ymin
            x2,y2 = polygon[1]  # xmin, ymax
            x3,y3 = polygon[2]  # xmax, ymax
            x4,y4 = polygon[3]  # xmax, ymin
            ltrb_label += f'table {scores[i]} {x1} {y1} {x3} {y3}\n'
            cv2.rectangle(img, (x1, y1), (x3, y3), (0, 255, 0), 3)

        print(f'ltrb_label: {ltrb_label}')

        # Save table cropped from original image
        # cv2.imwrite(os.path.join(save_path, basename), img)

        # Write into label txt file
        with open(os.path.join(save_path, f"{img_name}.txt"), 'w+') as label_file:
            label_file.write(ltrb_label)
        # print(f'{basename} processed successfully')

    avg_times.append(inference_time/len(img_paths))
    print(f'average inference time {inference_time/len(img_paths)}')

