## Installation
```
conda install pytorch==1.11.0 torchvision==0.12.0 torchaudio==0.11.0 cudatoolkit=11.3 -c pytorch
pip install -v -e .
pip install mmcv-full
```
## Download pretrained model and configuration
Model: [Download Here](https://hcmuteduvn-my.sharepoint.com/:u:/g/personal/khoi_nguyen2001_hcmut_edu_vn/Ec160J0ZtRZFrNA4-0zHqPoBS_txWW2IIA8FKc7QIcyc3g?e=bZvkVI)

Config: [Download Here](https://hcmuteduvn-my.sharepoint.com/:u:/g/personal/khoi_nguyen2001_hcmut_edu_vn/EZL40JrS5L9OnCT_Sd-nLHEByxyKl2pkULceTdK1K2kJAg?e=roreIw)

## Training
### Modify dataset path and model load path in configuration
- Change *pretrained* to point to pretrained model
- Change *data_root* to point to directory storing training data
- Note:
	+ data_root must has the following file structure:
	```
	data_root/
		annotations/
			instances_train2014.json
			instances_val2014.json
		logs/
		train2014/
		val2014/
	```
### Command
```
python tools/train.py path/to/cascade_rcnn_x101_32x4d_fpn_1x_coco.py
```
